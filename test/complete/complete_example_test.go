package test

import (
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// Test the complete VPC Example in https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc
func TestTerraformCompleteExample(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{

		TerraformDir: "../../examples/complete",
		Upgrade:      true,
		VarFiles:     []string{"complete.tfvars"},
		NoColor:      true,
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Expected values
	expectedVpcCidrBlock := "172.16.0.0/16"
	expectedVpcAdditionalCidrBlocks := []string{}
	expectedDynamicSubnetPrivateSubnetCidrs := []string{"172.16.0.0/19", "172.16.32.0/19"}
	expectedDynamicSubnetPublicSubnetCidrs := []string{"172.16.96.0/19", "172.16.128.0/19"}

	// Raw output actual values
	actualVpcCidrBlock := terraform.Output(t, terraformOptions, "vpc_cidr_block")
	actualVpcAdditionalCidrBlocks := terraform.OutputList(t, terraformOptions, "vpc_additional_cidr_blocks")
	actualDynamicSubnetPrivateSubnetCidrs := terraform.OutputList(t, terraformOptions, "dynamic_subnets_private_subnet_cidrs")
	actualDynamicSubnetPublicSubnetCidrs := terraform.OutputList(t, terraformOptions, "dynamic_subnets_public_subnet_cidrs")

	// Flossed actual values
	actualVpcCidrBlock = strings.Trim(actualVpcCidrBlock, "\"")

	// Assertions
	assert.Equal(t, expectedVpcCidrBlock, actualVpcCidrBlock)
	assert.Equal(t, expectedVpcAdditionalCidrBlocks, actualVpcAdditionalCidrBlocks)
	assert.Equal(t, expectedDynamicSubnetPrivateSubnetCidrs, actualDynamicSubnetPrivateSubnetCidrs)
	assert.Equal(t, expectedDynamicSubnetPublicSubnetCidrs, actualDynamicSubnetPublicSubnetCidrs)

}
