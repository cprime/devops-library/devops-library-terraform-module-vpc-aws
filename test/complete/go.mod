module gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc

go 1.13

require (
	github.com/aws/aws-sdk-go v1.33.0
	github.com/gruntwork-io/terratest v0.16.0
	github.com/stretchr/testify v1.5.1
)
