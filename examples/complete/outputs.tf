output "igw_id" {
  value = module.vpc.igw_id
}

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "vpc_cidr_block" {
  value = module.vpc.vpc_cidr_block
}

output "vpc_main_route_table_id" {
  value = module.vpc.vpc_main_route_table_id
}

output "vpc_default_network_acl_id" {
  value = module.vpc.vpc_default_network_acl_id
}

output "vpc_default_security_group_id" {
  value = module.vpc.vpc_default_security_group_id
}

output "vpc_default_route_table_id" {
  value = module.vpc.vpc_default_route_table_id
}

output "vpc_ipv6_association_id" {
  value = module.vpc.vpc_ipv6_association_id
}

output "vpc_ipv6_cidr_block" {
  value = module.vpc.ipv6_cidr_block
}

output "vpc_additional_cidr_blocks" {
  value = module.vpc.additional_cidr_blocks
}

output "vpc_additional_cidr_blocks_to_association_ids" {
  value = module.vpc.additional_cidr_blocks_to_association_ids
}

output "dynamic_subnets_public_subnet_cidrs" {
  value = module.subnets.public_subnet_cidrs
}

output "dynamic_subnets_private_subnet_cidrs" {
  value = module.subnets.private_subnet_cidrs
}
