#--------------------------------------------------------------------------------------------------------------
# Provider
#--------------------------------------------------------------------------------------------------------------
provider "aws" {
  region = var.region
}

#--------------------------------------------------------------------------------------------------------------
# VPC module
#--------------------------------------------------------------------------------------------------------------
module "vpc" {
  source     = "../../"
  cidr_block = "172.16.0.0/16"

  context = module.this.context
}

#--------------------------------------------------------------------------------------------------------------
# Subnets module
#--------------------------------------------------------------------------------------------------------------
module "subnets" {
  source  = "git::https://gitlab.com/cprime/devops-library/devops-library-terraform-module-dynamic-subnets-aws.git"

  availability_zones   = var.availability_zones
  vpc_id               = module.vpc.vpc_id
  igw_id               = module.vpc.igw_id
  cidr_block           = module.vpc.vpc_cidr_block
  nat_gateway_enabled  = false
  nat_instance_enabled = false

  context = module.this.context
}