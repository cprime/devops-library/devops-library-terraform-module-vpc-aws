<a name="unreleased"></a>
## [Unreleased]

### Documentation
- added localization directory for documentaiton ([#10](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/10))
- Definition of Done for a release ([#3](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/3))
- Working with git-chglog ([#7](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/7))
- Working with git-chglog ([#7](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/7))
- working with git-chglog

### Features
- refactored makefiles to external submodule ([#8](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/8))
- ci yml files refactored out to external repo ([#9](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/9))

### Test Coverage
- test ocverage documentation ([#12](https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/issues/12))


<a name="v0.0.0"></a>
## v0.0.0 - 2021-05-10

[Unreleased]: https://gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc/compare/v0.0.0...HEAD
