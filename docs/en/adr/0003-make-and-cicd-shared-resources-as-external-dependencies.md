# 3. make and cicd shared resources as external dependencies

Date: 2021-05-18

## Status

Accepted

## Context

The CID pipelines for DevOps library rely heavily on makefiles and ci pipelines expressed as yml files for GitLab CI.

Many of make targets and ci pipelines will be identical across projects, and indeed should be identical. Reuse will ensure the DRY (Don't Repeat Yourself) principle and ensure consistency across dependencies.

Having these as external dependencies will reduce rework, and there will be a canonical source for build functionality.

## Decision

We will create separate repositories for a makefile build harness and a cicd yml file build harness. We will allow local extension of these libraries in individual repositories.

The makefile build harness will be imported into DevOps Library repositories as a git submodule.

Build targets from the cicd build harness will be imported into individual projects as a 'remote include' over HTTPS.

The reason for these two different approaches is that GitLab CI cannot import external build targets as git submodules.
## Consequences

Having these dependencies in a central location and outside each repository will create an integration test overhead. It may also be more opaque to end-users regarding how the DevOps library integrations work 'under the hood.'

Having these dependencies and build targets in one place will ensure consistent quality with code owners available for review of any merge requests. 

This approach will also enforce consistency of implementation across DevOps library repositories.
