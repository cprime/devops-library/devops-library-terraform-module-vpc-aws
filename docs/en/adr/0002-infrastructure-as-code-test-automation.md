# 2. Infrastructure As Code Test Automation

Date: 2021-05-12

## Status

Accepted

## Context

The only way to create reliable, maintainable infrastructure code is to have a comprehensive suite of end-to-end acceptance tests. Automated tests are essential to provide confidence that infrastructure as code modules work as expected.

Provisioning non-trivial infrastructure should involve the use of many Terraform modules. Taking AWS as an example might include VPCs, ASGs, SGs, public/private subnets, etc. Each of these modules should be individually unit tested

Automated test suites should frequently run to ensure code under development meets its requirements. To this end, speed and simplicity of test execution are essential. 

Infrastructure as code is deployed to cloud environments. Cloud environments are in a constant state of development; cloud vendors change APIs and services daily. We must test infrastructure as code modules daily against AWS, Azure, and GCP to ensure ongoing compatibility. 

Changes and regressions of internal dependencies such as Terraform providers also require constant testing.

We must select a test automation framework to facilitate the development of integration test suites for all our infrastructure as code modules. 


### Design Alternatives

### [terratest](https://terratest.gruntwork.io/docs/testing-best-practices/alternative-testing-tools/)

Terratest is an open-source Go library that provides patterns and helper functions for testing infrastructure, with 1st-class support for Terraform, Packer, Docker, Kubernetes, AWS, GCP, and more.

#### [kitchen-terraform](https://newcontext-oss.github.io/kitchen-terraform/)

Kitchen-terraform is an open-source set of Ruby Test Kitchen plugins for testing Terraform configurations

#### [rspec-terraform](https://github.com/bsnape/rspec-terraform)

RSpec is a domain-specific language testing tool written in Ruby to test. It is a behavior-driven development framework. rspec-terraform is intended to smooth the creation, testing, and sharing of common Terraform modules. 

#### [serverspec](https://serverspec.org/)

With Serverspec, you can write RSpec tests for checking your servers are configured correctly.

Serverspec tests your servers’ actual state by executing command locally, via SSH, via WinRM, via Docker API and so on. So you don’t need to install any agent softwares on your servers and can use any configuration management tools, Puppet, Ansible, CFEngine, Itamae and so on.

But the true aim of Serverspec is to help refactoring infrastructure code.

#### [inspec](https://community.chef.io/tools/chef-inspec/)

Chef InSpec is an open-source framework for testing and auditing your applications and infrastructure. Chef InSpec works by comparing the actual state of your system with the desired state that you express in easy-to-read and easy-to-write Chef InSpec code.

#### [Goss](https://github.com/aelsabbahy/goss)

Goss is a YAML based serverspec alternative tool for validating a server’s configuration. It eases the process of writing tests by allowing the user to generate tests from the current system state. Once the test suite is written they can be executed, waited-on, or served as a health endpoint.

#### [awspec](https://github.com/k1LoW/awspec)

RSpec tests for AWS resources.

#### [Terraform acceptance testing framework](https://github.com/hashicorp/terraform/blob/main/.github/CONTRIBUTING.md#acceptance-tests-testing-interactions-with-external-services)

Terraform's unit test suite is self-contained, using mocks and local files to help ensure that it can run offline and is unlikely to be broken by changes to outside systems.

#### [ruby_terraform](https://github.com/infrablocks/ruby_terraform)

A simple wrapper around the Terraform binary to allow execution from within a Ruby program, RSpec test or Rakefile. 

### Terratest

The primary use case for most of the tools listed above relates to testing a single server or resource. Terratest takes a different approach in testing whether the infrastructure works as intended. 

Terratest provides a wrapper around terraform. This wrapper is lightweight and has no impact on the structure of the terraform code itself. We see the fact that terratest executes both the terraform code and verifies the resources deployed simultaneously to benefit productivity.

Terratest is written in golang, as is terraform; this means the engineers will only have to familiarize themselves with one language and framework. Go itself is a preferred language for our internal development due to its ease of dependency management and release packaging.

## Consequences

The go programming language is not widely adopted by many of our clients. This may prove a challenge in upskilling client teams.
